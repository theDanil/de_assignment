from PyQt5 import Qt
from PyQt5 import uic
import pyqtgraph as pg

import sys
from main import *

pg.setConfigOption('background', 'w')


class GraphWidget(Qt.QWidget):

    LINE_WIDTH = 5.0

    def __init__(self, xs, axis=("x", "y"), **kwargs):
        """
        :param xs: list of x's values
        :param axis: names of axis
        :param kwargs: any number of y's values(e.g. y1=[1, 2, 3], y2=[1, 3, 9], ...)
        """
        super().__init__()
        self.layout = Qt.QVBoxLayout(self)
        self.init_ui(xs, axis, kwargs)

    def init_ui(self, xs, axis, kwargs):
        self.plot = pg.PlotWidget()
        self.curve = self.plot.plot()
        colors = {"#f00": "red", "#00f": "blue", "#800080": "purple", "#0f0": "green"}
        colors_cycle = iter(colors.keys())
        top_label = ""
        for i, name in enumerate(kwargs):
            color = next(colors_cycle)
            top_label += "{} {}; ".format(name, colors[color])
            ys = kwargs[name]
            self.plot.plot(xs, ys, pen=pg.mkPen(color=color, width=self.LINE_WIDTH))
        self.plot.plotItem.setLabel("top", top_label)
        self.plot.plotItem.setLabel("bottom", axis[0])
        self.plot.plotItem.setLabel("left", axis[1])
        self.layout.addWidget(self.plot)


class ApproximatorApp(Qt.QMainWindow):

    def __init__(self, name):
        super().__init__()
        self.setWindowTitle(name)
        self.init_ui()

    def init_ui(self):
        self.resize(700, 900)
        self.main_widget = Qt.QTabWidget(self)
        self.main_widget.addTab(Qt.QWidget(), "solution")
        self.main_widget.addTab(Qt.QWidget(), "error-analyse")
        self.setCentralWidget(self.main_widget)
        self.main_widget.setTabsClosable(True)
        self.main_widget.setMovable(True)

        self.ui = uic.loadUi('settings_form.ui')
        self.settings = Qt.QDockWidget(self)
        self.settings.setMinimumWidth(0)
        self.settings.setFeatures(Qt.QDockWidget.DockWidgetMovable | Qt.QDockWidget.DockWidgetFloatable)
        self.settings.setWidget(self.ui)
        self.ui.plot_solution_button.clicked.connect(self.plot_solution)
        self.ui.plot_error_button.clicked.connect(self.plot_error_analyse)

        self.addDockWidget(Qt.Qt.LeftDockWidgetArea, self.settings)

    def plot_solution(self):
        x0 = self.ui.x0_value.value()
        X = self.ui.x_value.value()
        y0 = self.ui.y0_value.value()
        N = self.ui.n_value.value()

        grid = Grid(x0, X, y0, N)
        exact = ExactSolution(grid)
        eul = Euler(grid)
        impr_euler = ImprovedEuler(grid)
        rk = RungeKutta(grid)

        error_plot = GraphWidget(
            eul.get_xs(), axis=('x', 'error'), euler=eul.get_errors(exact.get_ys()),
                                           improved_euler=impr_euler.get_errors(exact.get_ys()),
                                           runge_kutta=rk.get_errors(exact.get_ys()))
        approx_plot = GraphWidget(
            eul.get_xs(), exact=exact.get_ys(), euler=eul.get_ys(),
            improved_euler=impr_euler.get_ys(), runge_kutta=rk.get_ys())

        grid_layout = Qt.QGridLayout()
        grid_layout.addWidget(approx_plot, 0, 0)
        grid_layout.addWidget(error_plot, 1, 0)
        graph_widget = Qt.QWidget()
        graph_widget.setLayout(grid_layout)

        self.main_widget.removeTab(0)
        self.main_widget.insertTab(0, graph_widget, "solution")
        self.main_widget.setCurrentWidget(graph_widget)

    def plot_error_analyse(self):
        x0 = self.ui.x0_value.value()
        X = self.ui.x_value.value()
        y0 = self.ui.y0_value.value()
        n0 = self.ui.n0_value.value()
        Nmax = self.ui.Nmax_value.value()

        n_range = range(n0, Nmax+1)

        grid = Grid(x0, X, y0)
        exact = ExactSolution(grid)
        eul = Euler(grid)
        impr_euler = ImprovedEuler(grid)
        rk = RungeKutta(grid)

        error_analyse_plot = GraphWidget(
            list(n_range), axis=('N', 'max error'), euler=eul.get_max_errors(n_range, exact),
            improved_euler=impr_euler.get_max_errors(n_range, exact),
            runge_kutta=rk.get_max_errors(n_range, exact))

        grid_layout = Qt.QGridLayout()
        grid_layout.addWidget(error_analyse_plot, 0, 0)
        graph_widget = Qt.QWidget()
        graph_widget.setLayout(grid_layout)

        self.main_widget.removeTab(1)
        self.main_widget.insertTab(1, graph_widget, "error-analyse")
        self.main_widget.setCurrentWidget(graph_widget)


if __name__ == '__main__':
    # run program
    app = Qt.QApplication([])
    mw = ApproximatorApp('ApproxApp')
    mw.show()
    sys.exit(app.exec_())
