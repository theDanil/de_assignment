from math import sin, cos, e


class Grid:

    def __init__(self, x0, X, y0, N=100):
        self.x0 = float(x0)
        self.X = float(X)
        self.y0 = float(y0)
        self.N = int(N)
        self.h = (X - x0) / (N - 1)

    def get_xs(self):
        x = self.x0
        xs = list()
        for i in range(self.N):
            xs.append(x)
            x += self.h
            if x > self.X:
                x = self.X
        return xs


class Approximator:

    def __init__(self, grid):
        self.grid = grid

    @staticmethod
    def func(x, y):
        """
        function of the derivative of y.
        """
        return cos(x) - y

    def get_xs(self):
        return self.grid.get_xs()

    def approximate(self, x, y):
        """
        Given Yn it returns Yn+1;
        Needs to be redefined.
        """
        return x + y

    def get_ys(self):
        y = self.grid.y0
        ys = list()
        xs = self.get_xs()
        for i in range(1, self.grid.N+1):
            ys.append(y)                # add Yn to y's list
            x = xs[i-1]                 # take i'th x
            y = self.approximate(x, y)  # compute Yn+1

        return ys

    def get_errors(self, exact_ys):
        approx_ys = self.get_ys()
        errors = [abs(y1_and_y2[0] - y1_and_y2[1]) for y1_and_y2 in zip(approx_ys, exact_ys)]
        return errors

    def get_max_errors(self, n_range, exact):
        result = list()
        for n in n_range:
            self.grid = Grid(x0=self.grid.x0, y0=self.grid.y0, X=self.grid.X, N=n)
            exact.grid = self.grid  # change grid in exact solution as well
            errors = self.get_errors(exact.get_ys())
            result.append(max(errors))

        return result


class Euler(Approximator):

    def approximate(self, x, y):
        return y + self.grid.h * self.func(x, y)


class ImprovedEuler(Approximator):

    def approximate(self, x, y):
        h = self.grid.h
        f = self.func
        return y + h/2 * (f(x, y) + f(x+h, y + h*f(x, y)))


class RungeKutta(Approximator):

    def approximate(self, x, y):
        h = self.grid.h
        f = self.func
        k1 = f(x, y)
        k2 = f(x + h/2, y + (h/2)*k1)
        k3 = f(x + h/2, y + (h/2)*k2)
        k4 = f(x + h, y + h*k3)
        return y + (h/6) * (k1 + 2*k2 + 2*k3 + k4)


class ExactSolution:

    def func(self, x):
        """
        Exact solution computed by me.
        """
        x0 = self.grid.x0
        y0 = self.grid.y0
        C = (y0 - cos(x0)/2 - sin(x0)/2) * e**x0  # first compute constant based on initial values
        return cos(x)/2 + sin(x)/2 + C / e**x

    def __init__(self, grid):
        self.grid = grid

    def get_xs(self):
        return self.grid.get_xs()

    def get_ys(self):
        return list(map(self.func, self.get_xs()))  # apply function func to every x in x's list




